# Tutoriel pour CoCalc

**Note:** Les informations qui suivent sont parcellaires et certainement biaisées. Tout le monde peut proposer un commit pour l'améliorer.

## Inscription

L'accès à CoCalc se fait à l'adresse suivante : <https://cocalc.math.univ-paris-diderot.fr>

Lors d'une première visite, il faut se créer un compte en cliquant sur `Sign In`

**Note:** Pour l'instant on est en phase de test et il faut se créer un compte "à la main". Idem si vous voulez "simuler" des étudiants. On peut envisager dans le futur que le lien serait fait avec la base de données des utilisateurs SG (profs + étudiants) et que chacun pourra s'identifier avec ses identifiants usuels (Moodle? ENT?)

![](img/signin.jpg)



## Gérer un projet/une classe

CoCalc fonctionne avec une structure assez (brouillonne) spécifique:

- Project
  - Course (=classe)
    - Assignment (=séance)

- Je n'ai franchement pas compris à quoi correspond un projet, mais c'est là que l'on définit qui sont les collaborateurs (=chargés de TP)
  - un *course* est un ensemble regroupant tous les documents relatifs à une classe. Il contient:
    - Des documents (fichiers de code etc)
    - Une liste d'étudiants (on pourrait ici séparer des promos en demi-groupes)
    - Des *assignements* qui correspondent à une séance de TP. En gros on va prendre un fichier et on va l'assigner à chaqueT étudiant, qui le verra apparaitre dans son environnement. On pourra alors voir ce qu'il fait, chatter directement avec lui/elle, et en temps voulu "ramasser les copies" si besoin.

On détaille maintenant comment créer et organiser tout ça:

### Créer un nouveau projet

- Cliquer sur `Create New Project` et lui donner un nom
  ![](img/newproject.jpg)
- Créer une classe (*course*) : Cliquer sur l'onglet `+New` , puis cliquer sur le bouton `Manage a course` 
  ![](img/newcourse.jpg)

- On ajoute des étudiants au cours, en tapant leur nom dans le cadre de droite. Ici, pour le tutoriel, j'ai créé en parallèle des étudiants fictifs (avec le formulaire Sign In mentionné plus haut). On peut espérer que plus tard ils seront tous enregistrés avec leurs adresses institutionnelles, et qu'il suffira de copier/coller une liste 
  ![](img/newstudent.jpg) 

  ![](img/list_student.jpg) 

  

### Lancer une séance de TP

- Il faut verser au cours votre sujet de TP que l'on va assigner aux étudiants. Pour cela, aller dans l'onglet `Assignments` , et cliquer à droite sur `Create an assugnment by directory name` , et taper un nom pour cette séance (penser à taper entrée pour faire apparaitre la boite de dialogue de création).
  ![](img/newassignment.jpg)
- Ouvrir l'assignment, pour qu'on y verse les documents nécessaires
  ![](img/open_assignment.jpg)
- Cliquer sur la petite boite `Upload` en haut à droite, ou bien glisser déposer un fichier au milieu de l'écran
  ![](img/upload.jpg)
  ![](img/upload2.jpg)
  - **Note:** il y a parfois un bug graphique qui fait qu'après avoir versé un fichier il n'apparait pas. Il suffit de rafraichir la page pour le voir apparaitre.
- une fois le dépot fait, le fichier fait partie de l'assignment. Il ne reste plus qu'à l'assigner aux élèves. Pour cela retourner à la page de l'assignment (en cliquant en haut sur l'onglet correspondant), puis cliquer sur Assign > Yes .
  ![](img/assign.jpg)
  - **Note:** il est possible d'assigner le sujet à un seul étudiant seulement. Mais je ne vois pas bien l'intérêt pour l'instant.
  - **Note:** Il est aussi possible de définir une deadline pour le rendu du devoir (set due date). Après cette date l'étudiant ne peut plus modifier son fichier j'imagine? Je n'ai pas testé.
- Voilà, les devoirs sont maintenant assignés aux étudiants, ils peuvent travailler de leur coté
  ![](img/assigned.jpg)



## Déroulement d'une séance

### Du point de vue de l'étudiant

- Lorsque l'étudiant se connecte, il voit les *courses* auquel il a été ajouté. Ici la page d'accueil de notre étudiant Jean Bombeur
  ![](img/student_board.jpg)
- Lorsqu'il clique sur le cours, il voit l'assignment `TP1` qui contient un fichier `sujet.ipynb`  Il peut alors cliquer sur le sujet, qui lance le programme (dans notre example : un notebook python)
  ![](img/tp_student_open.jpg)
- Il peut faire du code comme dans un notebook classique

### Du point de vue de l'enseignant

- Depuis la page de l'assignment (là où on a assigné le sujet) on peut "ouvrir" le dossier dans lequel travaille chaque étudiant
  ![](img/tp_prof_board.jpg)
- En ouvrant le fichier, l'enseignant peut voir ce que fait l'élève, en temps réel (il y a un curseur qui indique où est l'élève)
  ![](img/tp_prof_crossview.jpg)
- L'enseignant peut directement modifier le fichier de l'étudiant. Dans ce cas cela apparait en direct chez l'étudiant, qui voit également un curseur apparaitre:
  ![](img/tp_student_crossview.jpg)
- Il est également possible de parler directement à l'étudiant, via le chat (on l'ouvre via une icone en haut à droite)
  ![](img/tp_prof_chat.jpg)
- Il y a également la possibilité de lancer une conversation privée vidéo/audio (qui passe par jitsi). Je ne sais pas si c'est très pratique. 



# Autres détails

## Gestion de la mémoire

La création d'un *project* consomme environ 140Mo de RAM. Chaque notebook ouvert consomme un peu moins de 100Mo. Donc on pourrait penser que la mémoire totale est de 140 + (100 x nbstudents). 

Or ce n'est pas le cas : à chaque fois qu'on assigne un devoir à un étudiant, cela crée un projet parallèle (l'étudiant le voit avec comme titre *Prenom nom - titreprojet* ). Donc la consommation totale est doublée voire plus : (140+100) x nbstudents.

Peut-on faire mieux?

- Proposition de Frédéric : Faire 1 projet + mettre tout le monde en collaborateur. Chaque etudiant se cree un fichier à son nom, on gagne les 140Mo par etudiant et l'on a qu'un bouton d'information à surveiller pour la mémoire




## Sécurité

Chaque étudiant peut inviter un autre étudiant à travailler avec lui. Est-ce désirable? (Dans la page d'accueil, avant de joindre la classe, on peut "add people")

